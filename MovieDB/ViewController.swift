import UIKit

class ViewController: UIViewController {
    var movieInfos = [MovieInfo]()
    
    let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())

    override func viewDidLoad() {
        super.viewDidLoad()
        
        movieInfos.append(MovieInfo(title: "Notas perfectas 3", imageUrl: "https://i.imgur.com/xzgg3ka.jpg"))
        movieInfos.append(MovieInfo(title: "L.O.R.D ", imageUrl: "https://i.imgur.com/JrACvOs.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "https://i.imgur.com/8mfCmaP.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "https://i.imgur.com/Qa3PXKv.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "https://imgur.com/HTXXhOL.jpg"))
        movieInfos.append(MovieInfo(title: "American Pie", imageUrl: "https://imgur.com/nfXa7RR.jpg"))
        movieInfos.append(MovieInfo(title: "American Pie 2", imageUrl: "https://imgur.com/brKAkho.jpg"))
        movieInfos.append(MovieInfo(title: "American Pie 3", imageUrl: "https://imgur.com/HJaKAbp.jpg"))
        movieInfos.append(MovieInfo(title: "American Pie 4", imageUrl: "https://imgur.com/HHcxVoA.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "https://imgur.com/X4HR6ef.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "https://imgur.com/ASgHjXD.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "https://imgur.com/pzJthZJ.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "https://imgur.com/IMkzXKE.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "https://imgur.com/9wGK9O0.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "https://imgur.com/2jPqxkL.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "https://imgur.com/r1hAWKb.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "https://imgur.com/KhW8CBy.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "https://imgur.com/SXB9ety.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "http://imgur.com/oIOuqTw.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "http://imgur.com/z1FVkD5.jpg"))
        movieInfos.append(MovieInfo(title: "String", imageUrl: "http://imgur.com/91SXpSF.jpg"))

        
        collectionView.register(MoviePosterCell.self, forCellWithReuseIdentifier: "id")
        
        addCollectionView()
    }
    
    private func addCollectionView() {
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 10, right: 20)
        layout.itemSize = CGSize(width: 100, height: 200)
        
        collectionView.setCollectionViewLayout(layout, animated: true)
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.insetsLayoutMarginsFromSafeArea = true
        
        collectionView.backgroundColor = .black
        view.addSubview(collectionView)
        
        setupContraints()
    }
    
    private func setupContraints() {
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
            ])
    }


}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieInfos.count
    }
 
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "id", for: indexPath) as! MoviePosterCell
        
        let movieInfo = movieInfos[indexPath.row]
        
        
        DispatchQueue.global().async {
            if let url = URL(string: movieInfo.imageUrl), let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        cell.imageView.image = image
                    }
                }
            }
        }
        
        cell.composeView(withMovieName: movieInfo.title)
    
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

}


