import UIKit

class MoviePosterCell: UICollectionViewCell {
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    lazy var titleLabel: UILabel = {
        let titleLabel = UILabel(frame: .zero)
        titleLabel.textColor = .white
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        return titleLabel
    }()
    
    func composeView(withMovieName movieName: String) {
        addSubview(imageView)
        addSubview(titleLabel)
        
        titleLabel.text = movieName
        titleLabel.numberOfLines = 0
        titleLabel.sizeToFit()
        titleLabel.textAlignment = .center
        
        setupContraints()
    }

    private func setupContraints() {
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant: 100),
            imageView.heightAnchor.constraint(equalToConstant: 160),
            imageView.topAnchor.constraint(equalTo: topAnchor),
            
            titleLabel.widthAnchor.constraint(equalToConstant: 100),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
}

